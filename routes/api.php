<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});



    Route::post('login', 'Admin\Api\ApiAuthController@login');




Route::group(['middleware' => 'auth:api'], function() {
    Route::get('details', 'Admin\Api\ApiAuthController@details');
});
//Route::get('login', function (Request $request){
//   return $request->user();
//});

//Route::group(['auth:api' => 'user'], function() {
//
//    Route::get('user', 'Admin\Api\ApiAuthController@user');
//});

//Route::group(['prefix' => 'user'], function() {
//    Route::get('/', function() {
//        return response()->json(request()->user());
//    });
//});

//Route::middleware('auth:api')->get('/user', function (Request $request){
//   return $request->user();
//});

Route::post('register', 'Admin\Api\ApiAuthController@register');
Route::post('recover', 'Admin\Api\ApiAuthController@recover');
Route::post('change','Admin\Api\ApiAuthController@change');
Route::post('update/{id}','Admin\Api\ApiAuthController@update');



Route::get('coops','Admin\Api\CoopApiController@index');
Route::get('coops/{id}','Admin\Api\CoopApiController@show');

Route::get('coop-users', 'Admin\Api\CoopUserApiController@index');
Route::get('coop-users/{id}','Admin\Api\CoopUserApiController@show');
Route::get('coop-user-by-id/{id}', 'Admin\Api\CoopUserApiController@coopByUserId');


Route::get('coop-with-coopuser/{id}','Admin\Api\CoopApiController@coopByUserAndCoopUser');



Route::get('coop-by-user-id-coop-id/{userId}/{coopId}', 'Admin\Api\CoopUserApiController@coopUserByUserAndCoopId');
Route::get('coop-info-by-user-id/{id}', 'Admin\Api\CoopApiController@coopInfoByUserId');


Route::get('advertisements', 'Admin\Api\AdvertisementApiController@index');
Route::post('contact-us','Admin\Api\ContactApiController@sendContact');
