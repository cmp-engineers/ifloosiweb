<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoopUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coop_users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('payment_due');
            $table->date('payment_received');
            $table->string('payment');
            $table->string('status_payment');
            $table->string('status_received');
            $table->string('payout');
            $table->string('comment');
            $table->unsignedBigInteger('coop_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('admin_id');
            $table->foreign('coop_id')->references('id')->on('coops')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('admin_id')->references('id')->on('admins')->onDelete('cascade');
            $table->timestamps();

        });

//        Schema::create('coop_users', function (Blueprint $table){
//
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coop_users');
    }
}
