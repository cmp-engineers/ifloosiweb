<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(Model\Admin\Coop::class, function (Faker $faker) {
    return [
        //
        'name' => $faker->name,
        'description' => $faker->paragraph,
        'image' => $faker->imageUrl(250,250),
        'start_coop' => $faker->date('Y-m-d'),
        'end_coop' => $faker->date('Y-m-d'),
        'period' => $faker-> numberBetween(1,12),
        'members' => $faker->numberBetween(1,25),
        'amount' => $faker->randomNumber(4),
        'admin_id' => $faker->randomNumber(1),


    ];
});
