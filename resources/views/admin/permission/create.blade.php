@extends('admin.layouts.master')
@section('before-css')
    <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
    <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">


@endsection

@section('main-content')
    <div class="breadcrumb">
        <h1>@lang('permission.title')</h1>
{{--        <ul>--}}
{{--            <li><a href="">Form</a></li>--}}
{{--            <li>Basic</li>--}}
{{--        </ul>--}}
    </div>

    <div class="separator-breadcrumb border-top"></div>

    <div class="row">

        <div class="col-md-12">
            <div class="card mb-4">
                <div class="card-body">
                    <div class="card-title mb-3"> @lang('permission.permission')</div>
                    <form method="POST" action="{{ route('permission.store') }}">
                        @csrf
                        <div class="row">
                            <div class="col-md-6 form-group mb-3">
                                <label for="name"> @lang('permission.name')</label>
                                <input type="text" class="form-control form-control-rounded" id="name" placeholder="Enter permission name" name="name">
                            </div>

                            <div class="col-md-12">
                                <button class="btn btn-primary">@lang('permission.submit')</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>


@endsection

@section('page-js')
    <script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
    <script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>



@endsection

@section('bottom-js')
    <script src="{{asset('assets/js/form.basic.script.js')}}"></script>


@endsection
