@extends('admin.layouts.master')
@section('page-css')

    <link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
@endsection

@section('main-content')
    <div class="breadcrumb">
        <h1>@lang('note.title')</h1>
{{--        <ul>--}}
{{--            <li><a href="">UI Kits</a></li>--}}
{{--            <li>Datatables</li>--}}
{{--        </ul>--}}
    </div>
    <div class="separator-breadcrumb border-top"></div>

    <div class="row mb-4">
        <div class="col-md-12">
            <h4><a class="btn btn-primary" href="">@lang('note.add')</a></h4>
{{--            <p>DataTables is a plug-in for the jQuery Javascript library. It is a highly flexible tool, build upon the foundations of progressive enhancement, that adds all of these advanced features to any HTML table.</p>--}}
        </div>
    </div>
    <!-- end of row -->

    <div class="row mb-4">
        <div class="col-md-12 mb-4">
            <div class="card text-left">

                <div class="card-body">
{{--                    <h4 class="card-title mb-3">Zero configuration</h4>--}}
{{--                    <p>DataTables has most features enabled by default, so all you need to do to use it with your own ables is to call the construction function: $().DataTable();.</p>--}}
                    <div class="table-responsive">
                        <table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
                            <thead>
                            <tr>
                                <th>@lang('common.id')</th>
                                <th>@lang('note.name')</th>
{{--                                <th>{{ __('Content') }}</th>--}}
                                <th>@lang('common.edit') </th>
                                <th>@lang('common.delete')</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($notes as $note)
                                <tr>
                                    <td>{{ $loop->index + 1 }}</td>
                                    <td>{{ $note->name }}</td>
{{--                                    <td>{{ $note->content }}</td>--}}
                                    <td><a href="{{ route('note.edit', $note->id) }}"><button type="button" class="btn btn-raised ripple btn-raised-primary m-1">
                                                @lang('note.edit')</button></a></td>
                                    <td>

                                        <form method="POST" id="delete-form-{{$note->id}}"  action="{{ route('note.destroy', $note->id) }}" style="display: none">
                                            @csrf
                                            @method('DELETE')

                                        </form>


                                        <a  onclick="
                                                if (confirm('Are you sure , you want to delete this ?')) {
                                                document.getElementById('delete-form-{{$note->id}}').submit();
                                                } else {
                                                event.preventDefault();
                                                }"> <button type="button" class="btn btn-raised ripple btn-raised-danger m-1">

                                                @lang('note.delete')</button></a>

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>{{ __('S.No') }}</th>
                                <th>{{ __('Name') }}</th>
{{--                                <th>{{ __('Content') }}</th>--}}
                                <th>{{ __('Edit') }}</th>
                                <th>{{ __('Delete') }}</th>
                            </tr>
                            </tfoot>

                        </table>
                    </div>

                </div>
            </div>
        </div>
        <!-- end of col -->

        @endsection



        @section('page-js')

            <script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
            <script src="{{asset('assets/js/datatables.script.js')}}"></script>

@endsection
