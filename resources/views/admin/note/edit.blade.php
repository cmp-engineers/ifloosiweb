@extends('admin.layouts.master')
@section('before-css')
    <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
    <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">


@endsection

@section('page-css')
    <link rel="stylesheet" href="{{asset('assets/styles/vendor/quill.bubble.css')}}">
    <link rel="stylesheet" href="{{asset('assets/styles/vendor/quill.snow.css')}}">
@endsection

@section('main-content')
    <div class="breadcrumb">
        <h1>@lang('note.title')</h1>
        {{--        <ul>--}}
        {{--            <li><a href="">Form</a></li>--}}
        {{--            <li>Basic</li>--}}
        {{--        </ul>--}}
    </div>
    <div class="separator-breadcrumb border-top"></div>

    <div class="row">

        <div class="col-md-12">
            <div class="card mb-4">
                <div class="card-body">
                    <div class="card-title mb-3">@lang('note.add')</div>
                    <form method="POST" action="{{ route('note.store') }}" enctype="multipart/form-data">
                        @csrf
                        {{--                        <div class="row">--}}
                        <div class="col-md-12 form-group mb-3">
                            <label for="name"> @lang('common.name')</label>
                            <input type="text" class="form-control form-control-rounded" id="name" placeholder="Enter your note name" name="name" value="{{ $note->name }}">
                        </div>

                        <div class="col-md-12 form-group mb-3">
                            <div class="col-md-12 mb-4">
                                <div class="card text-left">

                                    <div class="card-body">
{{--                                        <h2>FULL EDITOR </h2>--}}
{{--                                        <p>By default all formats are enabled and allowed to exist within a Quill editor and can be configured with the formats option. This is separate from adding a control in the Toolbar. For example, you can configure Quill to allow--}}
{{--                                            bolded content to be pasted into an editor that has no bold button in the toolbar.</p>--}}
                                        <div class="mx-auto col-md-8">
                                            {{--                                            <div id="full-editor" >--}}
                                            <textarea id="full-editor1" name="content"  cols="30" rows="10">

                                                                {{ $note->content }}
                                                    </textarea>

                                            {{--                                            </div>--}}
                                        </div>
                                    </div>

                                </div>

                            </div>


                            <div class="col-md-12">
                                <button class="btn btn-primary">@lang('note.edit')</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>


@endsection

@section('page-js')

    <script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js"></script>
    <script src="{{asset('assets/js/vendor/quill.min.js')}}"></script>

    <script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
    <script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>




@endsection

@section('bottom-js')
    <script src="{{asset('assets/js/form.basic.script.js')}}"></script>
    <script src="{{asset('assets/js/quill.script.js')}}"></script>
    <script src="{{ asset('assets/js/ckeditor/ckeditor.js') }}"></script>

    <script>
        $(function () {
            // Replace the <textarea id="editor1"> with a CKEditor
            // instance, using default configuration.
            CKEDITOR.replace('full-editor1');
            //bootstrap WYSIHTML5 - text editor
            $(".textarea").wysihtml5();
        });
    </script>


@endsection
