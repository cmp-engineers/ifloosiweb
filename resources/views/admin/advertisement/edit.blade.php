@extends('admin.layouts.master')
@section('before-css')
    <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
    <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">


@endsection

@section('main-content')
    <div class="breadcrumb">
        <h1>@lang('advertisement.title')</h1>
{{--        <ul>--}}
{{--            <li><a href="">Form</a></li>--}}
{{--            <li>Basic</li>--}}
{{--        </ul>--}}
    </div>

    <div class="separator-breadcrumb border-top"></div>

    <div class="row">

        <div class="col-md-12">
            <div class="card mb-4">
                <div class="card-body">
                    <div class="card-title mb-3">@lang('advertisement.advertisement')</div>
                    <form method="POST" action="{{ route('advertisement.update', $advertisement->id) }}" enctype="multipart/form-data">
                        @csrf
                        @method('PATCH')
                        <div class="row">
                            <div class="col-md-6 form-group mb-3">
                                <label for="name"> @lang('advertisement.name')</label>
                                <input type="text" class="form-control form-control-rounded" id="name" placeholder="Enter your first name" name="name" value="{{$advertisement->name}}">
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="image"> @lang('advertisement.image')</label>
                                <input type="file" class="form-control form-control-rounded" id="content" placeholder="Upload your content" name="content">
                            </div>

                            <div class="col-md-12">
                                <button class="btn btn-primary">@lang('advertisement.edit_advertisement')</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>


@endsection

@section('page-js')
    <script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
    <script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>



@endsection

@section('bottom-js')
    <script src="{{asset('assets/js/form.basic.script.js')}}"></script>


@endsection
