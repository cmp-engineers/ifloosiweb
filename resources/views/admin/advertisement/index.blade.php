@extends('admin.layouts.master')
@section('page-css')

    <link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
@endsection

@section('main-content')
    <div class="breadcrumb">
        <h1>@lang('advertisement.title')</h1>
{{--        <ul>--}}
{{--            <li><a href="">UI Kits</a></li>--}}
{{--            <li>Datatables</li>--}}
{{--        </ul>--}}
    </div>
    <div class="separator-breadcrumb border-top"></div>

    <div class="row mb-4">
        <div class="col-md-12">
            <h4><a class="btn btn-primary" href="{{ route('advertisement.create') }}">@lang('advertisement.add_advertisement')</a></h4>
        </div>
    </div>
    <!-- end of row -->

    <div class="row mb-4">
        <div class="col-md-12 mb-4">
            <div class="card text-left">

                <div class="card-body">
{{--                    <h4 class="card-title mb-3">Zero configuration</h4>--}}
                    <div class="table-responsive">
                        <table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
                            <thead>
                            <tr>
                                <th>@lang('common.id')</th>
                                <th>@lang('advertisement.name')</th>
                                <th>@lang('advertisement.image')</th>
                                <td>@lang('common.edit')</td>
                                <td>@lang('common.delete')</td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($advertisements as $advertisement)
                                <tr>
                                    <td>{{ $loop->index + 1 }}</td>
                                    <td>{{ $advertisement->name }}</td>
                                    <td><img width="100px" height="100px"  src="{{ Storage::disk('local')->url($advertisement->content) }}" alt=""> </td>
                                    <td><a href="{{ route('advertisement.edit', $advertisement->id) }}"><button type="button" class="btn btn-raised ripple btn-raised-primary m-1">
                                                @lang('advertisement.edit_advertisement')</button></a></td>
                                    <td>

                                        <form method="POST" id="delete-form-{{$advertisement->id}}"  action="{{ route('advertisement.destroy', $advertisement->id) }}" style="display: none">

                                            @csrf
                                            @method('DELETE')

                                        </form>


                                        <a href="" onclick="
                                                if (confirm('Are you sure , you want to delete this ?')) {
                                                document.getElementById('delete-form-{{$advertisement->id}}').submit();
                                                } else {
                                                event.preventDefault();
                                                }"> <button type="button" class="btn btn-raised ripple btn-raised-danger m-1">

                                                @lang('advertisement.delete_advertisement')</button></a>

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>@lang('common.id')</th>
                                <th>@lang('advertisement.name')</th>
                                <th>@lang('advertisement.image')</th>
                                <td>@lang('common.edit')</td>
                                <td>@lang('common.delete')</td>

                            </tr>
                            </tfoot>

                        </table>
                    </div>

                </div>
            </div>
        </div>
        <!-- end of col -->

        @endsection



        @section('page-js')

            <script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
            <script src="{{asset('assets/js/datatables.script.js')}}"></script>

@endsection
