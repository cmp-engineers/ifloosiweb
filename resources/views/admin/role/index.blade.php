@extends('admin.layouts.master')
@section('page-css')

    <link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
@endsection

@section('main-content')
    <div class="breadcrumb">
        <h1>@lang('role.title')</h1>
{{--        <ul>--}}
{{--            <li><a href="">UI Kits</a></li>--}}
{{--            <li>Datatables</li>--}}
{{--        </ul>--}}
    </div>
    <div class="separator-breadcrumb border-top"></div>

    <div class="row mb-4">
        <div class="col-md-12">
            <h4><a class="btn btn-primary" href="{{ route('role.create') }}">@lang('role.add_role')</a></h4>
        </div>
    </div>
    <!-- end of row -->

    <div class="row mb-4">
        <div class="col-md-12 mb-4">
            <div class="card text-left">

                <div class="card-body">
                    <h4 class="card-title mb-3">@lang('role.role')</h4>
{{--                    <p><a class="btn btn-primary" href="{{ route('role.create') }}">@lang('role.add_role')</a></p>--}}
                    <div class="table-responsive">
                        <table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
                            <thead>
                            <tr>
                                <th>@lang('common.id')</th>
                                <th>@lang('role.role_name')</th>
{{--                                <th>{{ __('Roles') }}</th>--}}
                                <th>@lang('common.edit')</th>
                                <th>@lang('common.delete')</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($roles as $role)
                                <tr>
                                    <td>{{ $loop->index + 1 }}</td>
                                    <td>{{ $role->name }}</td>
{{--                                    <td>--}}
{{--                                        @foreach($permissions as $permission)--}}
{{--                                            {{ $permission->name }},--}}
{{--                                        @endforeach--}}
{{--                                    </td>--}}
                                    <td><a href="{{ route('role.edit', $role->id) }}"><button type="button" class="btn btn-raised ripple btn-raised-primary m-1">
                                                @lang('role.edit_role')</button></a></td>
                                    <td>

                                        <form method="POST" id="delete-form-{{$role->id}}"  action="{{ route('role.destroy', $role->id) }}" style="display: none">

                                            @csrf
                                            @method('DELETE')

                                        </form>


                                        <a onclick="
                                                if (confirm('Are you sure , you want to delete this ?')) {
                                                document.getElementById('delete-form-{{$role->id}}').submit();
                                                } else {
                                                event.preventDefault();
                                                }"> <button type="button" class="btn btn-raised ripple btn-raised-danger m-1">

                                                @lang('role.delete_role')</button></a>

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>@lang('common.id')</th>
                                <th>@lang('role.role_name')</th>
                                {{--                                <th>{{ __('Roles') }}</th>--}}
                                <th>@lang('common.edit')</th>
                                <th>@lang('common.delete')</th>
                            </tr>
                            </tfoot>

                        </table>
                    </div>

                </div>
            </div>
        </div>
        <!-- end of col -->
    </div>
        @endsection



        @section('page-js')

            <script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
            <script src="{{asset('assets/js/datatables.script.js')}}"></script>

@endsection
