@extends('admin.layouts.master')
@section('before-css')
    <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
    <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">


@endsection

@section('main-content')
    <div class="breadcrumb">
        <h1>@lang('admin.division')</h1>
        {{--        <ul>--}}
        {{--            <li><a href="">Form</a></li>--}}
        {{--            <li>Basic</li>--}}
        {{--        </ul>--}}
    </div>

    <div class="separator-breadcrumb border-top"></div>

    <div class="row">

        <div class="col-md-12">
            <div class="card mb-4">
                <div class="card-body">
                    <div class="card-title mb-3">@lang('admin.form')</div>
                    <form method="POST" action="{{ route('admin.profile-update', $admin->id) }}" >
                        @csrf
                        @method('PATCH')
                        <div class="row">
                            <div class="col-md-6 form-group mb-3">
                                <label for="name"> @lang('admin.name')</label>
                                <input type="text" class="form-control form-control-rounded" id="name" placeholder="Enter your first name" name="name" value="{{ $admin->name }}">
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="email"> @lang('admin.email')  </label>
                                <input type="email" class="form-control form-control-rounded" id="content" placeholder="email" name="email" value="{{ $admin->email }}">
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="mobile"> @lang('admin.mobile')  </label>
                                <input type="number" class="form-control form-control-rounded" id="content" placeholder="Mobile" name="mobile" value="{{ $admin->mobile }}">
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="name"> @lang('admin.password')  </label>
                                <input type="password" class="form-control form-control-rounded" id="content" placeholder="Password" name="password">
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="name">@lang('admin.password_confirm')</label>
                                <input type="password" class="form-control form-control-rounded" id="content" placeholder="Confirm password " name="password_confirmation">
                            </div>

                            <div class="col-md-12">
                                <button class="btn btn-primary">@lang('admin.submit')</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>


@endsection

@section('page-js')
    <script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
    <script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>



@endsection

@section('bottom-js')
    <script src="{{asset('assets/js/form.basic.script.js')}}"></script>


@endsection
