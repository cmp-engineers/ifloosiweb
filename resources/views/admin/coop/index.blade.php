@extends('admin.layouts.master')
@section('page-css')

    <link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
@endsection

@section('main-content')
    <div class="breadcrumb">
        <h1>@lang('coop.title')</h1>
{{--        <ul>--}}
{{--            <li><a href="">UI Kits</a></li>--}}
{{--            <li>Datatables</li>--}}
{{--        </ul>--}}
    </div>
    <div class="separator-breadcrumb border-top"></div>

    <div class="row mb-4">
        <div class="col-md-12">
            <h4><a class="btn btn-primary" href="{{ route('coop.create') }}">@lang('coop.add_coop')</a></h4>
        </div>
    </div>
    <!-- end of row -->

    <div class="row mb-4">
        <div class="col-md-12 mb-4">
            <div class="card text-left">

                <div class="card-body">
                    <h4 class="card-title mb-3">@lang('coop.coop')</h4>
{{--                    <p>DataTables has most features enabled by default, so all you need to do to use it with your own ables is to call the construction function: $().DataTable();.</p>--}}
                    <div class="table-responsive">
                        <table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
                            <thead>
                            <tr>
                                <th>@lang('common.id')</th>
                                <th>@lang('coop.name')</th>
                                <th>@lang('coop.description')</th>
                                <th>@lang('coop.start_date')</th>
                                <th>@lang('coop.end_date')</th>
                                <th>@lang('coop.period')</th>
                                <th>@lang('coop.members')</th>
                                <th>@lang('coop.amount') KWD</th>
                                <th>@lang('common.edit')</th>
                                <th>@lang('common.delete')</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($coops as $coop)
                            <tr>
                                <td>{{ $loop->index + 1 }}</td>
                                <td>{{ $coop->name }}</td>
                                <td>{{ $coop->description }}</td>
                                <td>{{ $coop->start_coop }}</td>
                                <td>{{ $coop->end_coop }}</td>
                                <td>{{ $coop->period }}</td>
                                <td>{{ $coop->members }}</td>
                                <td>{{ $coop->amount }}</td>
                                <td><a href="{{ route('coop.edit', $coop->id) }}"><button type="button" class="btn btn-raised ripple btn-raised-primary m-1">
                                            @lang('coop.edit_coop')</button></a></td>
                                <td>

                                    <form method="POST" id="delete-form-{{$coop->id}}"  action="{{ route('coop.destroy', $coop->id) }}" style="display: none">

                                        @csrf
                                        @method('DELETE')

                                    </form>


                                    <a  onclick="
                                            if (confirm('Are you sure , you want to delete this ?')) {
                                            document.getElementById('delete-form-{{$coop->id}}').submit();
                                            } else {
                                            event.preventDefault();
                                            }"> <button type="button" class="btn btn-raised ripple btn-raised-danger m-1">

                                         @lang('coop.delete_coop')</button></a>

                                </td>
                            </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>@lang('common.id')</th>
                                <th>@lang('coop.name')</th>
                                <th>@lang('coop.description')</th>
                                <th>@lang('coop.start_date')</th>
                                <th>@lang('coop.end_date')</th>
                                <th>@lang('coop.period')</th>
                                <th>@lang('coop.members')</th>
                                <th>@lang('coop.amount') KWD</th>
                                <th>@lang('common.edit')</th>
                                <th>@lang('common.delete')</th>
                            </tr>
                            </tfoot>

                        </table>
                    </div>

                </div>
            </div>
        </div>
        <!-- end of col -->
    </div>
        @endsection



        @section('page-js')

            <script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
            <script src="{{asset('assets/js/datatables.script.js')}}"></script>

        @endsection
