@extends('admin.layouts.master')
@section('before-css')
    <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
    <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">


@endsection

@section('main-content')
    <div class="breadcrumb">
        <h1>@lang('coop.division')</h1>
{{--        <ul>--}}
{{--            <li><a href="">Form</a></li>--}}
{{--            <li>Basic</li>--}}
{{--        </ul>--}}
    </div>

    <div class="separator-breadcrumb border-top"></div>

    <div class="row">

        <div class="col-md-12">
            <div class="card mb-4">
                <div class="card-body">
                    <div class="card-title mb-3">@lang('coop.info')</div>
                    <form method="POST" action="{{ route('coop.update', $coop->id) }}" enctype="multipart/form-data">
                        @csrf
                        @method('PATCH')
                        <div class="row">
                            <div class="col-md-6 form-group mb-3">
                                <label for="name"> @lang('coop.name')</label>
                                <input type="text" class="form-control form-control-rounded" id="name" placeholder="Enter your first name" name="name" value="{{ $coop->name }}">
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="name"> @lang('coop.description')  </label>
                                <input type="text" class="form-control form-control-rounded" id="description" placeholder="Enter your Description" name="description" value="{{ $coop->description }}">
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="file"> @lang('coop.image')  </label>
                                <input type="file" class="form-control form-control-file form-control-rounded" id="image" placeholder="upload your Image" name="image">
                                <div><img src="{{ Storage::disk('local')->url($coop->image) }}" width="150px" alt=""></div>
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="lastName2">@lang('coop.start_date')</label>
                                <input  id="picker2" type="date" class="form-control form-control-rounded"  placeholder="Start Date" name="start_coop" value="{{ $coop->start_coop }}">
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="lastName2">@lang('coop.end_date')</label>
                                <input id="picker2" type="date" class="form-control form-control-rounded"  placeholder="End Date" name="end_coop" value="{{ $coop->end_coop }}">
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="period">@lang('coop.period')</label>
                                <input type="text" class="form-control form-control-rounded" id="exampleInputEmail2"  placeholder="Enter Period" name="period" value="{{ $coop->period }}">
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="phone1">@lang('coop.members')</label>
                                <input type="text" class="form-control form-control-rounded" id="memebers" placeholder="Enter How many people" name="members" value="{{ $coop->members }}">
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="credit">@lang('coop.amount')</label>
                                <input type="number" class="form-control form-control-rounded" id="credit" placeholder="Enter Amount" name="amount" value="{{ $coop->amount }}">
                            </div>

                            <div class="col-md-12">
                                <button class="btn btn-primary">@lang('coop.edit_coop')</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>


@endsection

@section('page-js')
    <script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
    <script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>



@endsection

@section('bottom-js')
    <script src="{{asset('assets/js/form.basic.script.js')}}"></script>


@endsection
