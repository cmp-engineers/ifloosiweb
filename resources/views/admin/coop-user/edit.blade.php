@extends('admin.layouts.master')
@section('before-css')
    <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
    <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">


@endsection

@section('main-content')
    <div class="breadcrumb">
        <h1>@lang('coop-user.title')</h1>
{{--        <ul>--}}
{{--            <li><a href="">Form</a></li>--}}
{{--            <li>Basic</li>--}}
{{--        </ul>--}}
    </div>

    <div class="separator-breadcrumb border-top"></div>

    <div class="row">

        <div class="col-md-12">
            <div class="card mb-4">
                <div class="card-body">
                    <div class="card-title mb-3">@lang('coop-user.add')</div>
                    <form method="POST" action="{{ route('coop-user.update', $coopUser->id) }}" >
                        @csrf
                        @method('PATCH')
                        <div class="row">

                            <div class="col-md-6 form-group mb-3">
                                <label for="coops">@lang('common.select')</label>
                                <select class="form-control form-control-rounded" name="coop_id" id="coop_id">
                                    @foreach($coops as $coop)
                                        @if(auth()->id() == $coop->admin_id)
                                            <option value="{{ $coop->id }}">{{ $coop->name }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="users">@lang('coop-user.user')</label>
                                <select class="form-control form-control-rounded" name="user_id" id="user_id">
                                    @foreach($users as $user)
                                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="payment due">@lang('coop-user.payment_due')</label>
                                <input type="date" class="form-control form-control-rounded" id="picker2" name="payment_due" value="{{ $coopUser->payment_due }}" >
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="payment due">@lang('coop-user.payment')</label>
                                <input type="number" class="form-control form-control-rounded" id="payment" name="payment" value="{{ $coopUser->payment }}">
                            </div>


                            <div class="col-md-6 form-group mb-3">
                                <label for="status">@lang('coop-user.status_payment')</label>
                                <select class="form-control form-control-rounded" name="status_payment" id="status_payment">
                                    <option value="" disabled selected> @lang('common.select') </option>
                                    <option value="not-paid">Not Paid - لم يتم الدفع</option>
                                    <option value="paid">تم الدفع - paid</option>
                                    <option value="delay">تأخير في الدفع - Delay</option>
                                </select>
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="payment due">@lang('coop-user.payment_received')</label>
                                <input type="date" class="form-control form-control-rounded" id="picker2" name="payment_received"  value="{{ $coopUser->payment_received }}">
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="status">@lang('coop-user.status_received') </label>
                                <select class="form-control form-control-rounded" name="status_received" id="status" required>
                                    <option value="" disabled selected> @lang('common.select')</option>
                                    <option value="payment received"> Payment Received</option>
                                    <option value="payment not received">Not Received Payment</option>
                                    <option value="delay">Delay</option>
                                </select>
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for=" Comment">@lang('coop-user.comment')</label>
                                <input type="text" class="form-control form-control-rounded" id="comment" name="comment" value="{{ $coopUser->comment }}" >
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="payout">@lang('coop-user.payout')</label>
                                <input type="number" class="form-control form-control-rounded" id="payout" name="payout" value="{{ $coopUser->payout }}" >
                            </div>

                            <div class="col-md-12">
                                <button class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>


@endsection

@section('page-js')
    <script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
    <script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>



@endsection

@section('bottom-js')
    <script src="{{asset('assets/js/form.basic.script.js')}}"></script>


@endsection
