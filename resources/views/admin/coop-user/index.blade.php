@extends('admin.layouts.master')
@section('page-css')

    <link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
@endsection

@section('main-content')
    <div class="breadcrumb">
        <h1>@lang('coop-user.title')</h1>
{{--        <ul>--}}
{{--            <li><a href="">UI Kits</a></li>--}}
{{--            <li>Datatables</li>--}}
{{--        </ul>--}}
    </div>
    <div class="separator-breadcrumb border-top"></div>

    <div class="row mb-4">
        <div class="col-md-12">
            <h4><a class="btn btn-primary" href="{{ route('coop-user.create') }}">@lang('coop-user.add')</a></h4>
        </div>
    </div>
    <!-- end of row -->

    <div class="row mb-4">
        <div class="col-md-12 mb-4">
            <div class="card text-left">

                <div class="card-body">
                    <h4 class="card-title mb-3">@lang('coop-user.name')</h4>
                    <div class="table-responsive">
                        <table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
                            <thead>
                            <tr>
                                <th>@lang('common.id')</th>
                                <th>@lang('coop-user.coop_name')</th>
                                <th>@lang('coop-user.name')</th>
                                <th>@lang('coop-user.payment_due')</th>
                                <th>@lang('coop-user.payment')</th>
                                <th>@lang('coop-user.payout')</th>
                                <th>@lang('coop-user.status_received')</th>
                                <th>@lang('coop-user.comment')</th>
                                <th>@lang('common.edit')</th>
                                <th>@lang('common.delete')</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($coopUsers as $coopUser)
                                <tr>
                                    <td>{{ $loop->index + 1 }}</td>
                                    <td>{{ \App\Model\Admin\Coop::whereId($coopUser->coop_id)->value('name')}}</td>
                                    <td>{{ \App\Model\User\User::whereId($coopUser->user_id)->value('name') }}</td>
                                    <td>{{ $coopUser->payment_due }} </td>
                                    <td>{{ $coopUser->payment }} KWD</td>
                                    <td>{{ $coopUser->payout }} KWD</td>
                                    <td>{{ $coopUser->status_payment}}</td>
                                    <td>{{ $coopUser->comment }}</td>
                                    <td><a href="{{ route('coop-user.edit', $coopUser->id) }}"><button type="button" class="btn btn-raised ripple btn-raised-primary m-1">
                                                @lang('coop-user.edit')</button></a></td>
                                    <td>

                                        <form method="POST" id="delete-form-{{$coopUser->id}}"  action="{{ route('coop-user.destroy', $coopUser->id) }}" style="display: none">

                                            @csrf
                                            @method('DELETE')

                                        </form>


                                        <a  onclick="
                                                if (confirm('Are you sure , you want to delete this ?')) {
                                                document.getElementById('delete-form-{{$coopUser->id}}').submit();
                                                } else {
                                                event.preventDefault();
                                                }"> <button type="button" class="btn btn-raised ripple btn-raised-danger m-1">

                                                @lang('coop-user.delete')</button></a>

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>@lang('common.id')</th>
                                <th>@lang('coop-user.coop_name')</th>
                                <th>@lang('coop-user.name')</th>
                                <th>@lang('coop-user.payment_due')</th>
                                <th>@lang('coop-user.payment')</th>
                                <th>@lang('coop-user.payout')</th>
                                <th>@lang('coop-user.status_received')</th>
                                <th>@lang('coop-user.comment')</th>
                                <th>@lang('common.edit')</th>
                                <th>@lang('common.delete')</th>
                            </tr>
                            </tfoot>

                        </table>
                    </div>

                </div>
            </div>
        </div>
        <!-- end of col -->

        @endsection



        @section('page-js')

            <script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
            <script src="{{asset('assets/js/datatables.script.js')}}"></script>

@endsection
