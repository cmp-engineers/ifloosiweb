@extends('admin.layouts.master')
@section('before-css')
    <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
    <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">


@endsection

@section('main-content')
    <div class="breadcrumb">
        <h1>Basic</h1>
        <ul>
            <li><a href="">Form</a></li>
            <li>Basic</li>
        </ul>
    </div>

    <div class="separator-breadcrumb border-top"></div>

    <div class="row">

        <div class="col-md-12">
            <div class="card mb-4">
                <div class="card-body">
                    <div class="card-title mb-3">Form Inputs Rounded</div>
                    <form method="POST" action="{{ route('admin.update', $admin->id) }}" >
                        @csrf
                        @method('PATCH')
                        <div class="row">
                            <div class="col-md-6 form-group mb-3">
                                <label for="name"> name</label>
                                <input type="text" class="form-control form-control-rounded" id="name" placeholder="Enter your name" name="name" value="{{ $admin->name }}">
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="name"> Email  </label>
                                <input type="email" class="form-control form-control-rounded" id="content" placeholder="email" name="email" value="{{ $admin->email }}">
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="name"> mobile  </label>
                                <input type="number" class="form-control form-control-rounded" id="content" placeholder="Mobile" name="mobile" value="{{ $admin->mobile }}">
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="name"> Password  </label>
                                <input type="password" class="form-control form-control-rounded" id="content" placeholder="Password" name="password" >
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="name"> Password Confirmation </label>
                                <input type="password" class="form-control form-control-rounded" id="content" placeholder="Confirm password " name="password_confirmation">
                            </div>

                            <div class="col-md-12">
                                <button class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>


@endsection

@section('page-js')
    <script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
    <script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>



@endsection

@section('bottom-js')
    <script src="{{asset('assets/js/form.basic.script.js')}}"></script>


@endsection
