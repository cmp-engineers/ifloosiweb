<?php

return [

    'admin' => 'المستخدمين',
    'add_admin' => 'اضافة مستخدم جديد',
    'admin_index' => 'المستخدمين',
    'dashboard' => 'الصفحة الرئيسية',
    'coop' => 'الجمعيات',
    'add_coop' => 'اضافة جمعية',
    'coop_index' => 'الجميعات',
    'permissions' => 'الصلاحيات',
    'add_permission' => 'اضافة صلاحية',
    'roles' => 'المهام',
    'add_role' => 'اضافة مهام',
    'role_index' => 'المهمات',
    'advertisement' => 'الاعلانات',
    'add_advertisement' => 'اضافة اعلان',
    'coop_user' => 'الاعضاء',
    'add_coop_user' => 'اضافة عضو للجمعية',
    'note' => 'مذكرة',
    'add_note' => 'اضافة مذكرة',
    'note_index' => 'المذكرات',

];
