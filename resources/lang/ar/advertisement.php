<?php

return [

    // add advertisement

    'title' => 'قسم الاعلانات',
    'advertisement' => 'الاعلانات',
    'submit' => 'تسجيل اعلان',
    'image' => 'صورة الاعلان',
    'name' => 'اسم الاعلان',
    'add_advertisement' => 'اضافة اعلان جديد',

    // edit advertisement

    'edit_advertisement' => 'تعديل الاعلان',
    'delete_advertisement' => 'حذف الاعلان',

];
