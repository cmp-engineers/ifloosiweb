<?php

return [

    // add admin

    'title' =>  'قسم المستخدمين',
    'admins' => 'المستخدمين',
    'division' => 'قسم اضافة مستخدم جديد' ,
    'add_admin' => 'اضافة مستخدم جديد',
    'form' => 'تسجيل مستخدم جديد',
    'name' => 'اسم المستخدم',
    'email' => 'البريد الالكتروني',
    'mobile' => 'رقم الهاتف',
    'password' => 'كلمة السر',
    'password_confirm' => 'تأكيد كلمة السر',
    'submit' => 'تسجيل مستخدم',
    'type' => 'نوع المستخدم',

    // admin table

    'edit-admin' => 'تعديل المستخدم',
    'delete-admin' => 'حذف المستخدم',





];
