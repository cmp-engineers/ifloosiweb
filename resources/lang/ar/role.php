<?php

return [

    //add role
    'title' => 'قسم المهام',
    'role' => 'المهام',
    'role_name' => 'اسم المهام',
    'add_role' => 'اضافة مهام جديدة',
    'submit' => 'تسجيل مهام',

    // role index
    'edit_role' => 'تعديل مهام' ,
    'delete_role' => 'حذف مهام' ,



];
