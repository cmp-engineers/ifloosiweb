<?php

return [

    // add permission

    'title' => 'قسم الصلاحيات',
    'permission' => 'الصلاحيات',
    'permission_name' => 'اسم الصلاحية',
    'add_permission' => 'اضافة صلاحية جديدة',
    'name' => 'الاسم',
    'submit' => 'تسجيل صلاحية',


    // permission index
     'edit_permission' => 'تعديل صلاحية',
    'delete_permission' => 'مسح صلاحية'

];
