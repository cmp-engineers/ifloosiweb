<?php

return [


        'admin' => 'Admin',
        'add_admin' => 'Add Admin',
        'dashboard' => 'Dashboard',
        'admin_index' => 'Admin',
        'coop' => 'Coop',
        'add_coop' => 'Add Coop ',
        'coop_index' => 'Coop',
        'permissions' => 'permissions',
        'add_permission' => 'add permission',
        'roles' => 'roles',
        'add_role' => 'add role',
        'role_index' => 'roles',
        'advertisement' => 'advertisement',
        'add_advertisement' => 'add advertisement',
        'coop_user' => 'coop users',
        'add_coop_user' => 'add coop user',
        'note' => 'note',
        'add_note' => 'add note',
        'note_index' => 'notes',

];
