<?php

// add advertisement
 return [
     'title' => 'قسم الاعلانات',
     'advertisement' => 'الاعلانات',
     'submit' => 'تسجيل اعلان',
     'image' => 'صورة الاعلان',
     'name' => 'اسم الاعلان',

     // edit advertisement

     'edit_advertisement' => 'تعديل الاعلان',
     'delete_advertisement' => 'حذف الاعلان',
 ];
