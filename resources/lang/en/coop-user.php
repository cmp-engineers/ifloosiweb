<?php

return [

    // add coop-user
    'title' => 'Coop Users',
    'add' => 'Add new user',
    'select' => 'select coop',
    'payment_due' => 'Payment due',
    'user' => 'select user for coop',
    'payment' => 'payment' ,
    'status_payment' => 'Payment status',
    'comment' => 'Comment',
    'payment_received' => 'Payment received',
    'status_received' => 'Status payment received',
    'payout' => 'Payout',
    'submit' => 'Submit',

    'coop_user_name' => 'Coop user name',

];
