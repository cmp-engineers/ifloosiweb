<?php

return [

    'title' => 'Coops',
    'coop' => 'coops',
    'add_coop' => 'add new coop' ,
    'division' => 'Coop',
    'info' => 'coop info',
    'name' => 'Name',
    'description' => 'Description',
    'image' => 'Image',
    'start_date' => 'Start Date',
    'end_date' => 'End Date',
    'period' => 'Period',
    'members' => 'Members',
    'amount' => 'Amount',
    'submit' => 'Submit',

    'edit_coop' => 'Edit',
    'delete_coop' => 'Delete'

];
