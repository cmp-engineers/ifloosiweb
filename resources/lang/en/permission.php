<?php

return [

    'title' => 'Permissions',
    'permission' => 'Permission',
    'permission_name' => 'Permission name',
    'add_permission' => 'add new permission',
    'name' => 'name',
    'submit' => 'submit',


    // permission index
     'edit_permission' => 'edit permission',
    'delete_permission' => 'delete permission'

];
