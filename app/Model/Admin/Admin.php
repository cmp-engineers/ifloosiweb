<?php

namespace App\Model\Admin;

use App\Notifications\AdminResetPasswordNotification;
use Illuminate\Auth\Notifications\ResetPassword as ResetPasswordNotification;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Collection;
use Spatie\Permission\Traits\HasRoles;
use App\Http\Controllers\Admin\Auth;

class Admin extends Authenticatable
{
    //
    use Notifiable,HasRoles;
   // protected $guard = 'admin';
//    protected  $guard_name= 'admin';
  //  protected $guarded = 'admin';
 // protected  $guard = 'web';
  // protected $guard_name = 'web';

    protected $fillable = ['name' , 'mobile' , 'email', 'password'];

    protected $table = 'admins';


    public function coops()
    {
        return $this->hasMany(Coop::class);
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new AdminResetPasswordNotification($token));
    }
}
