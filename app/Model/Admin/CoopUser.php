<?php

namespace App\Model\Admin;

use App\Model\User\User;
use Illuminate\Database\Eloquent\Model;

class CoopUser extends Model
{
    //
    public function coop()
    {
        return $this->belongsTo(Coop::class,'coop_id','id');
    }

    public function coopsAndUser()
    {
        return $this->belongsToMany(CoopUser::class,'coop_user','coop_id','user_id')->withPivot('coop_id','user_id');
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }


}
