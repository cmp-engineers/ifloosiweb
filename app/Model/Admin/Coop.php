<?php

namespace App\Model\Admin;

use App\Model\User\User;
use Illuminate\Database\Eloquent\Model;

class Coop extends Model
{
    //
//    public function users()
//    {
//        return $this->hasMany(CoopUser::class, 'user_id','id');
//    }

        public function users()
        {
           // return $this->belongsToMany(User::class,'coop_user','coop_id','user_id');
            return $this->belongsToMany(User::class);
        }


    public function coopUser()
    {
        return $this->hasMany(CoopUser::class,'coop_id','id');
    }

    protected $fillable = [

        'name',
        'image',
        'start_coop',
        'end_coop',
        'period',
        'members',
        'amount',
        'admin_id',

    ];

    public function user() {
        return $this->belongsToMany(User::class,'coop_user');
    }

}
