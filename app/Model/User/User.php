<?php

namespace App\Model\User;

use App\Model\Admin\Coop;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','mobile'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Find the user instance for the given username.
     *
     * @param  string  $username
     * @return \App\User
     */

    public function findForPassPort($username)
    {
        return $this->where('mobile', $username)->first();
    }

    public function coops()
    {
        return $this->belongsToMany(Coop::class);
    }

    public function coop() {
        return $this->belongsToMany(Coop::class,'coop_user');
    }

}
