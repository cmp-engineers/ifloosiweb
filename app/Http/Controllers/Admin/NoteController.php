<?php

namespace App\Http\Controllers\Admin;

use App\Model\Admin\Note;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class NoteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        //
        $admins = Auth::user();
        if ($admins->hasRole('super-admin')) {
            $notes = Note::all();
        } else {
         $notes = Note::where('admin_id' , auth()->id())->get();
        }


        return view('admin.note.index', compact('notes'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        //
        return view('admin.note.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        //

            $this->validate( $request,[

                'name' => 'required',
                'content' => 'required',
            ]);

        $note = new Note;

       $note->name = request('name');
        //$note->name = $request->name;
        $note->content = request('content');
       // $note->content = $request->content;
        $note->admin_id = auth()->id();

        $note->save();

        return redirect(route('note.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function edit($id)
    {
        //
        $note = Note::findOrFail($id);
        return view('admin.note.edit',compact('note'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        //
        $note = Note::findOrFail($id);

        $note->name = $request->name;

        if (!empty($request->input('content')))
        {
            $note->content = $request->input('content');
        }

        $note->save();

        return redirect(route('note.index'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        //
        Note::where('id',$id)->delete();

        return redirect()->back();
    }
}
