<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Resources\CoopResource;
use App\Http\Resources\CoopsResource;
use App\Model\Admin\Coop;
use App\Model\User\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CoopApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return CoopsResource
     *
     */
    public function index()
    {
        //

        $coops = Coop::paginate();
        return new CoopsResource($coops);

    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return CoopResource
     */
    public function show($id)
    {
        //
        $coop = Coop::find($id);
        return new CoopResource($coop);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function coopByUserAndCoopUser($id)
    {
        $coopByUserAndCoopUSer = Coop::with(['coopUser'])->where(['id' => $id])->get();

        return new CoopsResource($coopByUserAndCoopUSer);
    }


    public function coopInfoByUserId($id)
    {
        $user = User::findOrFail($id);
        return new CoopsResource($user->coops);
    }

}
