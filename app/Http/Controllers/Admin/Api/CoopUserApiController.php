<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Resources\CoopsResource;
use App\Http\Resources\CoopsUsersResource;
use App\Http\Resources\CoopUserResource;
use App\Model\Admin\CoopUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CoopUserApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $coopUsers = CoopUser::paginate();

        return new CoopsUsersResource($coopUsers);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

        $coopUser = CoopUser::find($id);

        return new CoopUserResource($coopUser);
    }

    /**
     * Display the CoopUser by user id
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function coopByUserId($id)
    {
        $coopByUserId = CoopUser::where(['user_id' => $id])->get();
       // $coopByUserId = CoopUser::with('coop')->where(['user_id' => $id])->get();

        return new CoopUserResource($coopByUserId);

    }





    public function coopUserByUserAndCoopId($userId, $coopId) {

        $coopUserByUserIdAndCoopId = CoopUser::where(['user_id' => $userId , 'coop_id' => $coopId])->get();

        return new CoopUserResource($coopUserByUserIdAndCoopId);

    }




}
