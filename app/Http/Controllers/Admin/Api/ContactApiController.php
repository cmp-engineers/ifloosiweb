<?php

namespace App\Http\Controllers\Admin\Api;

use App\Model\Admin\Contact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactApiController extends Controller
{


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function sendContact(Request $request)
    {
        //
        $contact = Contact::create([
           'name'   => $request->name,
           'mobile' => $request->mobile,
           'email' => $request->email,
           'content' => $request->content,
        ]);

        return response([
           'success' => 'contact has been sent'
        ],202);

    }


}
