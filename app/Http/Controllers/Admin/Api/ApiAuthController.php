<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Requests\ApiRegisterRequest;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\RecoverPasswordRequest;
use App\Mail\UserPasswordPin;
use App\Model\User\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Exception\BadResponseException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;

class ApiAuthController extends Controller {

    //
    public $successStatus = 200;
    private $CLIENT_ID = 2;
    private $CLIENT_SECRET = 'IL8t7ExbGfFyqAiBXk1BFuFgdJOcPi9w30IYqYxW';


    private $unwantedPins = [
        111111, 222222, 333333, 444444, 555555, 666666, 777777, 888888, 999999
    ];

    private function inWanted($pin)
    {
        return in_array($pin, $this->unwantedPins);
    }

    /*
     * @var $user User
     */

    public function register(ApiRegisterRequest $request)
    {

        $user = User::create([

            'name' => $request->name,
            'mobile' => $request->mobile,
            'email' => $request->email,
            'password' => Hash::make($request->password),

        ]);

        //  return $user;

        $http = new Client();

        $response = $http->post('https://ifloosi.com/oauth/token', [

            'form_params' => [
                'grant_type' => 'password',
                'client_id' => $this->CLIENT_ID,
                'client_secret' => $this->CLIENT_SECRET,
                'username' => $request->mobile,
                'password' => $request->password,
                'scope' => '',
            ],

        ]);

        return json_decode((string) $response->getBody(), true);
    }

//    public function login(Request $request)
//    {
//        try {
//
//            $http = new \GuzzleHttp\Client;
//
//            $response = $http->post('http://ifloosi.test/oauth/token',[
//
//                    'form_params' => [
//
//                        'grant_type' => 'password',
//                        'client_id' => $this->CLIENT_ID,
//                        'client_secret' => $this->CLIENT_SECRET,
//                        'username' => $request->mobile,
//                        'password' => $request->password,
//                        'scope' => '',
//                    ],
//            ]);
//            return $response->getBody();
//
//        } catch (BadResponseException $e) {
//
//            return response()->json('errors', $e->getCode());
//
//        }
//    }

    public function login(Request $request)
    {
        // return $request;
        // $credentails = $request->only(['email', 'password']);
        $credentails = $request->only(['mobile', 'password']);
       // $authAtempt = auth()->attempt($credentails);
        $authattempt = Auth::attempt($credentails);

//        if (Auth::attempt(['mobile' => \request('mobile') , 'password' => request('password')])) {
//            $auth_user = Auth::user();
//            $user = User::find($auth_user->id);
//         // dd($user->createToken('myApp')->accessToken);
//
//        }


        if (!$authattempt)
        {
            return response([
                'error' => 'forbidden',
                'message' => 'check your username and password'
            ], 403);
        }

        $http = new Client();
        $response = $http->post('https://ifloosi.com/oauth/token', [
            'form_params' => [
                'grant_type' => 'password',
                'client_id' => $this->CLIENT_ID,
                'client_secret' => $this->CLIENT_SECRET,
                'username' => $request->mobile, //auth()->user()->email,
                'password' => $request->password, //auth()->user()->password,
                'scope' => '',
            ],
        ]);


      //  return json_decode((string) $response->getBody(), true);
        //$tokenResult = $user->createToken('MyApp')->accessToken;
        //$token = $tokenResult->token;
        // $token = \Auth::user()->createToken('Token')->accessToken;
         return response([

            // 'user_id' => auth()->id(),
             'user' => \auth()->user(),
          //   'access_token' => $token,
             'body' => json_decode((string) $response->getBody(), true),

         ]);

    }


    public function details(Request $request)
    {
     return response()->json($request->user());
    }

//    public function details()
//    {
//        $user = Auth::user();
//        return response()->json($user, 200);
//    }


    /*
     * @recover password
     */

    public function recover(RecoverPasswordRequest $request) {

        $user = User::where(['email' => $request->email])->first();

        if (is_null($user)) {

            return response([
                'error' => 'email does not match our records'
            ],404);

        }

        // send the recover password six digit code to that email
        $pin = rand(111111,999999);
        foreach ($this->unwantedPins as $unwantedPin) {

            if ($unwantedPin == $pin) {
                $pin = rand(111111,999999);
            }

        }

        // send email

        $user->pin = $pin;
        $user->save();

        Mail::to($user)->queue(new UserPasswordPin($pin));

        return response([
            'message' => 'a six digit pin number has been sent to your email'
        ], 200);

    }

    public function change(ChangePasswordRequest $request) {

        // find the user by pin
        $user = User::where(['pin' => $request->pin])->first();

        if (is_null($user)) {

            return response([
                'error' => 'user pin incorrect'
            ], 404);

        }

        $user->password = Hash::make($request->input('password'));
        $user->pin = null;
        $user->save();

        return response([
            'message' => 'Please login with your new password'
        ],202);

    }

    public function update($id , Request $request)
    {

        $user = User::where(['id' =>$id] )->first();

        if (empty($user)) {
            return response([
               'error'  => true,
                'message' => 'user not found',

            ],404);
        }

            if ($request->has('access_token')) {

                if ($request->input('name') != null) {
                    $user->name = $request->name;
                }

                if ($request->input('email') != null) {
                    $user->email = $request->email;
                }

                if ($request->input('mobile') != null) {
                    $user->mobile = $request->mobile;
                }

                if ($request->input('password') != null) {
                    $user->password = Hash::make($request->password);
                }
            }






            $user->update();

            return response([
                'Success' => 'your profile has been updated',
            ],202);

    }

}
