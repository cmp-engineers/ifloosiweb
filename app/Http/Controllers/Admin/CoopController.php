<?php

namespace App\Http\Controllers\Admin;

use App\Model\Admin\Admin;
use App\Model\Admin\Coop;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CoopController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    //
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        //
        $admins = Auth::user();
        if ($admins->hasRole('super-admin')) {
              $coops =  Coop::all();
        } else {
            $coops = Coop::where('admin_id', auth()->id())->get(); // we are calling all vendors
        }

        //return $coops;


        return view('admin.coop.index', compact('coops'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.coop.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        //

            $coop = new Coop;
            $coop->name = $request->name;
            $coop->description= $request->description;
            $coop->start_coop = Carbon::parse($request->start_coop);
            $coop->end_coop   = Carbon::parse($request->end_coop);
            $coop->period = $request->period;
            $coop->members = $request->members;
            $coop->amount = $request->amount;

            $coop->admin_id = Auth::id();

            if ($request->hasFile('image')) {
                $coop->image = $request->image->store('public');
            }

            $coop->save();


            return redirect(route('coop.index'));


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $coop = Coop::findOrFail($id);
        return view('admin.coop.edit', compact('coop'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $coop = Coop::findOrFail($id);

        $coop->name = $request->name;
        $coop->description= $request->description;
        $coop->start_coop = Carbon::parse($request->start_coop);
        $coop->end_coop   = Carbon::parse($request->end_coop);
        $coop->period = $request->period;
        $coop->members = $request->members;
        $coop->amount = $request->amount;


        if ($request->hasFile('image')) {
            $coop->image = $request->image->store('public');
        }

        $coop->update();

        return redirect(route('coop.index'));




    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)

    {
        //
        Coop::where('id', $id)->delete();

        return redirect()->back();

    }


}
