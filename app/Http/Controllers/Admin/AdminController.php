<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\AdminRequest;
use App\Model\Admin\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        //
        $admins = Admin::all();
        return view('admin.admin.index', compact('admins'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        //
        $roles = Role::get();
        return view('admin.admin.create',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(AdminRequest $request)
    {
        //

        $admin = new Admin();

        $admin->name = $request->name;
        $admin->email = $request->email;
        $admin->mobile = $request->mobile;
        $admin->password = Hash::make($request->password);

        $roles = $request->role;
        if (isset($roles)) {
            foreach ($roles as $role) {
                $role_r = Role::where('id', '=', $role)->firstOrFail();
                $admin->assignRole($role_r);
            }
        }


        //TODO: maybe remove it or not check it later
        $admin->SuperAdmin = $request->SuperAdmin = 0;


        $admin->save();

        return redirect(route('admin.home'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        //
        $admin = Admin::find($id);
        return view('admin.admin.edit',compact('admin'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        //
        $admin = Admin::where('id', $id)->first();

        $admin->name = $request->name;
        $admin->mobile = $request->mobile;
        $admin->email = $request->email;

        if ($request->password != null) {
            $admin->password =  bcrypt($request->password);;
        }

        $admin->update();

        return redirect(route('admin.home'));

    }



    public function profileEdit($id)
    {

        $admin = Admin::findOrFail($id);
        return view('admin.profile.edit',compact('admin'));
    }

    public function profileUpdate(Request $request,$id)
    {
        $admin = Admin::where('id', $id)->first();

        $admin->name = $request->name;
        $admin->mobile = $request->mobile;
        $admin->email = $request->email;

        if ($request->password != null) {
            $admin->password = $request->password;
        }

        $admin->update();

        return redirect(route('admin.home'));
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        //
        Admin::where('id', $id)->delete();

        return redirect()->back();

    }
}
