<?php

namespace App\Http\Controllers\Admin;

use App\Model\Admin\Coop;
use App\Model\Admin\Coop_User;
use App\Model\Admin\CoopUser;
use App\Model\User\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CoopUserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        //

        //$coops = Coop::where(['admin_id' => auth()->user()->id ]);
        $coops = Coop::all();
        $users = User::all();
        $coopUsers = CoopUser::all();
        return view('admin.coop-user.index',compact('coops', 'users', 'coopUsers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        //
        $users = User::all();
        $coops  = Coop::all();

        return view('admin.coop-user.create', compact('users', 'coops'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $coopUser = new CoopUser;

//        $this->validate($request, [
//
//            'payment_due' => 'required',
//            'coop_id' => 'required',
//            'user_id' => 'required',
//            'amount' => 'required',
//            'status' => 'required',
//        ]);

        $coopUser->payment_due           = Carbon::parse($request->payment_due);
        $coopUser->payment_received        = Carbon::parse($request->payment_received);
        $coopUser->coop_id               = $request->coop_id;
        $coopUser->user_id               = $request->user_id;
        $coopUser->payment               = $request->payment;
        $coopUser->status_payment        = $request->status_payment;
        $coopUser->status_received       = $request->status_received;
        $coopUser->payout                = $request->payout;
        $coopUser->comment               = $request->comment;
        $coopUser->admin_id              = auth()->user()->id;

        //return $coopUser;


       // dd($request->all());

      //  $coopUser->coopsAndUser()->attach(\request('coop'));

     // var_dump($dd);
        //$coopUser->coops_()->sync($request->coop_id);

        //Coop_User::create(['coop_id' =>  $coopUser->coop_id, 'user_id' =>  $coopUser->user_id ]);

        $coopUser->save();
        $coopUser->coopsAndUser()->sync( array(
            1 => array( 'coop_id' => $request->coop_id, 'user_id' => $request->user_id ),));

//        $coopUser->coops_()->sync($request->coop_id);
//        $coopUser->users_()->sync( $request->user_id);


//        $coop_user = new Coop_User();
//        $coopUser->coop_id = $request->coop_id;
//        $coopUser->user_id = $request->user_id;
//        $coop_user->save();


       // return $coopUser;

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $coopUser = CoopUser::findOrFail($id);
        $coops = Coop::all();
        $users = User::all();
        return view('admin.coop-user.edit', compact('coopUser', 'coops', 'users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $coopUser = CoopUser::findOrFail($id);

        $coopUser->payment_due           = Carbon::parse($request->payment_due);
        $coopUser->payment_receiv        = Carbon::parse($request->payment_received);
        $coopUser->coop_id               = $request->coop_id;
        $coopUser->user_id               = $request->user_id;
        $coopUser->payment               = $request->payment;
        $coopUser->status_payment        = $request->status_payment;
        $coopUser->status_received       = $request->status_received;
        $coopUser->payout                = $request->payout;
        $coopUser->comment               = $request->comment;

        $coopUser->admin_id              = auth()->user()->id;

        $coopUser->save();

        return redirect(route('coop-user.index'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        CoopUser::where('id',$id)->delete();

        return redirect()->back();
    }
}
